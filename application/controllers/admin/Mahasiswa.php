<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * 
	 */							
	class Mahasiswa extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->model("mahasiswa_model");
			$this->load->library("form_validation");
		}

		public function index()
		{
			$data["mahasiswa"]=$this->mahasiswa_model->join('mahasiswa','jurusan','prodi','jurusan.id_jurusan=mahasiswa.jurusan_mhs','prodi.id_prodi=mahasiswa.prodi_mhs','left');
			$this->load->view("admin/mahasiswa/list",$data);
		}

		public function add()
		{
			$data["jurusan"]=$this->mahasiswa_model->fetch_jurusan();
			$mahasiswa=$this->mahasiswa_model;
			$validation=$this->form_validation;
			$validation->set_rules($mahasiswa->rules());

			if($validation->run()){
				$mahasiswa->save();
				$this->session->set_flashdata('success','Berhasil disimpan');
				redirect(site_url('admin/mahasiswa'));
			}

			$this->load->view("admin/mahasiswa/new_form",$data);
		}

		public function edit($id=null)
		{
			if(!isset($id)) redirect('admin/mahasiswa');
			$data["jurusan"]=$this->mahasiswa_model->fetch_jurusan();
			$mahasiswa=$this->mahasiswa_model;
			$validation=$this->form_validation;
			$validation->set_rules($mahasiswa->rules());

			if($validation->run()){
				$mahasiswa->update();
				$this->session->set_flashdata('success','Berhasil disimpan');
				redirect(site_url('admin/mahasiswa'));
			}

			$data["mahasiswa"]=$mahasiswa->joinWhere('mahasiswa','jurusan','prodi','jurusan.id_jurusan=mahasiswa.jurusan_mhs','prodi.id_prodi=mahasiswa.prodi_mhs',$id);
			if(!$data["mahasiswa"]) show_404();

			$this->load->view("admin/mahasiswa/edit_form",$data);
		}

		public function delete($id=null)
		{
			if(!isset($id)) show_404();

			if($this->mahasiswa_model->delete($id)){
				redirect(site_url('admin/mahasiswa'));
			}
		}

		function fetch_prodi()
		{
			if($this->input->post('jurusan_mhs'))
		{
			echo $this->mahasiswa_model->fetch_prodi($this->input->post('jurusan_mhs'));
		}
 }
	}

 ?>