<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	/**
	 * 
	 */							
	class Prodi extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			$this->load->model("prodi_model");
			$this->load->library("form_validation");
		}

		public function index()
		{
			$data["prodi"]=$this->prodi_model->join();
			$this->load->view("admin/prodi/list",$data);
		}

		public function add()
		{
			$data["jurusan"]=$this->prodi_model->fetch_jurusan();
			$prodi=$this->prodi_model;
			$validation=$this->form_validation;
			$validation->set_rules($prodi->rules());

			if($validation->run()){
				$prodi->save();
				$this->session->set_flashdata('success','Berhasil disimpan');
				redirect(site_url('admin/prodi'));
			}

			$this->load->view("admin/prodi/new_form",$data);
		}

		public function edit($id=null)
		{
			if(!isset($id)) redirect('admin/prodi');
			$data["jurusan"]=$this->prodi_model->fetch_jurusan();
			$prodi=$this->prodi_model;
			$validation=$this->form_validation;
			$validation->set_rules($prodi->rules());

			if($validation->run()){
				$prodi->update();
				$this->session->set_flashdata('success','Berhasil disimpan');
				redirect(site_url('admin/prodi'));
			}

			$data["prodi"]=$prodi->joinWhere('prodi','jurusan','jurusan.id_jurusan=prodi.id_jurusan',$id);
			if(!$data["prodi"]) show_404();

			$this->load->view("admin/prodi/edit_form",$data);
		}

		public function delete($id=null)
		{
			if(!isset($id)) show_404();

			if($this->prodi_model->delete($id)){
				redirect(site_url('admin/prodi'));
			}
		}
	}

 ?>