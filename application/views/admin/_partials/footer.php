<footer class="main-footer">
    <strong>Copyright <?php echo SITE_NAME ." ". Date('Y') ?></a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
    </div>
  </footer>