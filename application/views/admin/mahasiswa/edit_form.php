<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<!-- Card  -->
				<div class="card mb-3">
					<div class="card-header">

						<a class="btn btn-sm bg-gradient-primary" href="<?php echo site_url('admin/mahasiswa/') ?>"><i class="fas fa-arrow-left"></i>
							Back</a>
					</div>
					<div class="card-body">

						<form action="" method="post" enctype="multipart/form-data">
						<!-- Note: atribut action dikosongkan, artinya action-nya akan diproses 
							oleh controller tempat vuew ini digunakan. Yakni index.php/admin/mahasiswa/edit/ID --->

							<input type="hidden" name="id_mhs" value="<?php echo $mahasiswa->id_mhs?>" />

							<div class="form-group">
								<label for="nama">Nama*</label>
								<input class="form-control <?php echo form_error('nama_mhs') ? 'is-invalid':'' ?>"
								 type="text" name="nama_mhs" placeholder="Nama Mahasiswa" value="<?php echo $mahasiswa->nama_mhs ?>"/>
								<div class="invalid-feedback">
									<?php echo form_error('nama_mhs') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="nim">NIM*</label>
								<input class="form-control <?php echo form_error('nim_mhs') ? 'is-invalid':'' ?>"
								 type="text" name="nim_mhs" placeholder="NIM Mahasiswa" value="<?php echo $mahasiswa->nim_mhs ?>"/>
								<div class="invalid-feedback">
									<?php echo form_error('nim_mhs') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="kelas">Kelas*</label>
								<input class="form-control <?php echo form_error('kelas_mhs') ? 'is-invalid':'' ?>"
								 type="text" name="kelas_mhs" placeholder="Kelas Mahasiswa" value="<?php echo $mahasiswa->kelas_mhs ?>"/>
								<div class="invalid-feedback">
									<?php echo form_error('kelas_mhs') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="jurusan">Jurusan*</label>
								<select name="jurusan_mhs" id="jurusan" class="custom-select" required>
									<option value="<?php echo $mahasiswa->jurusan_mhs ?>" selected>(Default) <?php echo $mahasiswa->nama_jurusan ?></option>
									<?php 
										foreach ($jurusan as $row) {
											echo '<option value="'.$row->id_jurusan.'">'.$row->nama_jurusan.'</option>';
										}
									 ?>
								</select>
							</div>

							<div class="form-group">
								<label for="prodi">Prodi</label>
								<select name="prodi_mhs" id="prodi" class="custom-select" required>
									<option value="<?php echo $mahasiswa->prodi_mhs ?>">(Default) <?php echo $mahasiswa->nama_prodi ?></option>
								</select>
							</div>


							<div class="form-group">
								<label for="alamat">Alamat*</label>
								<input class="form-control <?php echo form_error('alamat_mhs') ? 'is-invalid':'' ?>"
								 type="text" name="alamat_mhs" placeholder="Alamat Mahasiswa" value="<?php echo $mahasiswa->alamat_mhs ?>"/>
								<div class="invalid-feedback">
									<?php echo form_error('alamat_mhs') ?>
								</div>
							</div>
							
							<div class="form-group">
								<label for="telepon">Nomor Telepon*</label>
								<input class="form-control <?php echo form_error('telp_mhs') ? 'is-invalid':'' ?>"
								 type="text" name="telp_mhs" placeholder="Telepon Mahasiswa" value="<?php echo $mahasiswa->telp_mhs ?>"/>
								<div class="invalid-feedback">
									<?php echo form_error('telp_mhs') ?>
								</div>
							</div>

							<input class="btn btn-sm bg-gradient-success" type="submit" name="btn" value="Save" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* required fields
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->

		<?php $this->load->view("admin/_partials/scrolltop.php") ?>
		<?php $this->load->view("admin/_partials/js.php") ?>
		<script>
			$('#jurusan').change(function(){
				var jurusan_mhs = $('#jurusan').val();
				if(jurusan_mhs != '')
				{
					$.ajax({
						url:"<?php echo site_url('admin/mahasiswa/fetch_prodi'); ?>",
						method:"POST",
						data:{jurusan_mhs:jurusan_mhs},
						success:function(data)
						{
							$('#prodi').html(data);
						}
					});
				}
				else
				{
					$('#prodi').html('<option value="">Pilih Prodi</option>');
				}
			});
		</script>

</body>

</html>