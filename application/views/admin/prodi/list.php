<!DOCTYPE html>
<html lang="en">
<!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') ?>">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body class="hold-transition sidebar-mini sidebar-collapse">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<!-- DataTables -->
				<div class="card">
					<div class="card-header">
						<h4>Data Prodi</h4>
						<a class="btn bg-gradient-success" href="<?php echo site_url('admin/prodi/add') ?>"><i class="fas fa-plus"></i> Data Baru</a>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="table_mhs" class="table table-bordered table-hover" >
								<thead class="bg-gradient-info">
									<tr>
										<th>No.</th>
										<th>Nama Prodi</th>
										<th>Kode Prodi</th>
										<th>Jurusan</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1;foreach ($prodi as $prodi): ?>
									<tr>
										<td>
											<?php echo $i++ ?>
										</td>
										<td>
											<?php echo $prodi->nama_prodi ?>
										</td>
										<td>
											<?php echo $prodi->kode_prodi ?>
										</td>
										<td>
											<?php 
												$this->prodi_model->data_null($prodi->nama_jurusan,'Jurusan')
											?>
										</td>
										<td width="250">
											<a href="<?php echo site_url('admin/prodi/edit/'.$prodi->id_prodi) ?>"
											 class="btn btn-sm bg-gradient-primary"><i class="fas fa-edit"></i> Edit</a>
											<a onclick="deleteConfirm('<?php echo site_url('admin/prodi/delete/'.$prodi->id_prodi) ?>')"
											 href="#!" class="btn btn-sm text-danger"><i class="fas fa-trash"></i> Hapus</a>
										</td>
									</tr>
									<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php $this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>
	<?php $this->load->view("admin/_partials/js.php") ?>
	<script>
		function deleteConfirm(url)
		{
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}
	</script>
	<script>
		$(function () {
		    $("#table_mhs").DataTable({
		      "responsive": true,
		      "autoWidth": false,
		    });
  		});
	</script>

</body>
</html>