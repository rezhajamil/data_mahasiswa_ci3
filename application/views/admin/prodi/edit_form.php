<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<!-- Card  -->
				<div class="card mb-3">
					<div class="card-header">

						<a class="btn btn-sm bg-gradient-primary" href="<?php echo site_url('admin/prodi/') ?>"><i class="fas fa-arrow-left"></i>
							Back</a>
					</div>
					<div class="card-body">

						<form action="" method="post" enctype="multipart/form-data">
						<!-- Note: atribut action dikosongkan, artinya action-nya akan diproses 
							oleh controller tempat vuew ini digunakan. Yakni index.php/admin/prodi/edit/ID --->

							<input type="hidden" name="id_prodi" value="<?php echo $prodi->id_prodi?>" />

							<div class="form-group">
								<label for="nama">Nama Prodi*</label>
								<input class="form-control <?php echo form_error('nama_prodi') ? 'is-invalid':'' ?>"
								 type="text" name="nama_prodi" placeholder="Nama Prodi" value="<?php echo $prodi->nama_prodi ?>"/>
								<div class="invalid-feedback">
									<?php echo form_error('nama_prodi') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="kode">Kode prodi*</label>
								<input class="form-control <?php echo form_error('kode_prodi') ? 'is-invalid':'' ?>"
								 type="text" name="kode_prodi" placeholder="Kode Prodi" value="<?php echo $prodi->kode_prodi ?>"/>
								<div class="invalid-feedback">
									<?php echo form_error('kode_prodi') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="jurusan">Jurusan*</label>
								<select name="id_jurusan" id="jurusan" class="custom-select" required>
									<option value="<?php echo $prodi->id_jurusan ?>" selected>(Default) <?php echo $prodi->nama_jurusan ?></option>
									<?php 
										foreach ($jurusan as $row) {
											echo '<option value="'.$row->id_jurusan.'">'.$row->nama_jurusan.'</option>';
										}
									 ?>
								</select>
							</div>

							<input class="btn btn-sm bg-gradient-success" type="submit" name="btn" value="Save" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* required fields
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->

		<?php $this->load->view("admin/_partials/scrolltop.php") ?>
		<?php $this->load->view("admin/_partials/js.php") ?>
</body>

</html>