<!DOCTYPE html>
<html lang="en">
<!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') ?>">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body class="hold-transition sidebar-mini sidebar-collapse">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<!-- DataTables -->
				<div class="card">
					<div class="card-header">
						<h4>Data Jurusan</h4>
						<a class="btn bg-gradient-success" href="<?php echo site_url('admin/jurusan/add') ?>"><i class="fas fa-plus"></i> Data Baru</a>
					</div>
					<div class="card-body">
						<div class="table-responsive">
							<table id="table_jurusan" class="table table-bordered table-hover" >
								<thead class="bg-gradient-info">
									<tr>
										<th>No.</th>
										<th>Nama Jurusan</th>
										<th>Kode Jurusan</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1;foreach ($jurusan as $jurusan): ?>
									<tr>
										<td>
											<?php echo $i++ ?>
										</td>
										<td>
											<?php echo $jurusan->nama_jurusan ?>
										</td>
										<td>
											<?php echo $jurusan->kode_jurusan ?>
										</td>
										<td width="250">
											<a href="<?php echo site_url('admin/jurusan/edit/'.$jurusan->id_jurusan) ?>"
											 class="btn btn-sm bg-gradient-primary"><i class="fas fa-edit"></i> Edit</a>
											<a onclick="deleteConfirm('<?php echo site_url('admin/jurusan/delete/'.$jurusan->id_jurusan) ?>')"
											 href="#!" class="btn btn-sm text-danger"><i class="fas fa-trash"></i> Hapus</a>
										</td>
									</tr>
									<?php endforeach; ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container-fluid -->

			<!-- Sticky Footer -->
			<?php $this->load->view("admin/_partials/footer.php") ?>

		</div>
		<!-- /.content-wrapper -->

	</div>
	<!-- /#wrapper -->


	<?php $this->load->view("admin/_partials/scrolltop.php") ?>
	<?php $this->load->view("admin/_partials/modal.php") ?>
	<?php $this->load->view("admin/_partials/js.php") ?>
	<script>
		function deleteConfirm(url)
		{
			$('#btn-delete').attr('href', url);
			$('#deleteModal').modal();
		}
	</script>
	<script>
		$(function () {
		    $("#table_jurusan").DataTable({
		      "responsive": true,
		      "autoWidth": false,
		    });
  		});
	</script>

</body>
</html>