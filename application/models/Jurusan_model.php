<?php 
	/**
	 * 				
	 */
	class Jurusan_model extends CI_Model
	{
		private $_table="jurusan";

		public $id_jurusan;
		public $nama_jurusan;
		public $kode_jurusan;

		public function rules()
		{
			return[
				[
					'field'=>'nama_jurusan',
					'label'=>'Nama Jurusan',
					'rules'=>'required'
				],
				[
					'field'=>'kode_jurusan',
					'label'=>'Kode Jurusan',
					'rules'=>'required'
				],
			];
		}

		public function getAll()
		{
			return $this->db->get($this->_table)->result();

		}

		public function getById($id)
		{
			return $this->db->get_where($this->_table,["id_jurusan"=>$id])->row();
		}

		public function save()
		{
			$post=$this->input->post();
			$this->nama_jurusan=$post["nama_jurusan"];
			$this->kode_jurusan=$post["kode_jurusan"];
			return $this->db->insert($this->_table,$this);
		}

		public function update()
		{
			$post=$this->input->post();
			$this->id_jurusan=$post["id_jurusan"];
			$this->nama_jurusan=$post["nama_jurusan"];
			$this->kode_jurusan=$post["kode_jurusan"];
			return $this->db->update($this->_table,$this,array('id_jurusan'=> $post['id_jurusan']));
		}

		public function delete($id)
		{
			return $this->db->delete($this->_table,array("id_jurusan"=>$id));
		}
	}

 ?>