<?php 
	/**
	 * 				
	 */
	class Prodi_model extends CI_Model
	{
		private $_table="prodi";

		public $id_prodi;
		public $nama_prodi;
		public $kode_prodi;
		public $id_jurusan;

		public function rules()
		{
			return[
				[
					'field'=>'nama_prodi',
					'label'=>'Nama Prodi',
					'rules'=>'required'
				],
				[
					'field'=>'kode_prodi',
					'label'=>'Kode Prodi',
					'rules'=>'required'
				],
				[
					'field'=>'id_jurusan',
					'label'=>'Jurusan',
					'rules'=>'required'
				],
			];
		}

		public function getAll()
		{
			// $this->db->order_by('id_mhs','desc');
			return $this->db->get($this->_table)->result();

		}

		public function getById($id)
		{
			return $this->db->get_where($this->_table,["id_prodi"=>$id])->row();
		}

		public function join()
		{
			return $this->db->from($this->_table)
			->join('jurusan','jurusan.id_jurusan=prodi.id_jurusan','left')
			->order_by('prodi.id_jurusan','ASC')
			->get()
			->result();
		}

		public function joinWhere($tbl,$tbljoin,$join,$where)
		{
			// $this->db->order_by('id_mhs','desc');
			// $this->db->select('*');
			return $this->db
			->join($tbljoin,$join)
			->get_where($tbl,["id_prodi"=>$where])
			->row();
		}

		public function save()
		{
			$post=$this->input->post();
			$this->nama_prodi=$post["nama_prodi"];
			$this->kode_prodi=$post["kode_prodi"];
			$this->id_jurusan=$post["id_jurusan"];
			return $this->db->insert($this->_table,$this);
		}

		public function update()
		{
			$post=$this->input->post();
			$this->id_prodi=$post["id_prodi"];
			$this->nama_prodi=$post["nama_prodi"];
			$this->kode_prodi=$post["kode_prodi"];
			$this->id_jurusan=$post["id_jurusan"];
			return $this->db->update($this->_table,$this,array('id_prodi'=> $post['id_prodi']));
		}

		public function delete($id)
		{
			return $this->db->delete($this->_table,array("id_prodi"=>$id));
		}

		public function fetch_jurusan()
		{
		 	$this->db->order_by("id_jurusan", "ASC");
		  	$query = $this->db->from('jurusan');
		    // ->join('mahasiswa','mahasiswa.jurusan_mhs=jurusan.id_jurusan','left');
		  	return $query->get()->result();
		}

		public function data_null($output,$text)
		{
			if($output==null)
			{
				echo "<b>Silahkan isi Data ".$text."</b>";
			}
			else
			{
				echo $output;
			}
		}
	}

 ?>