<?php 
	/**
	 * 				
	 */
	class Mahasiswa_model extends CI_Model
	{
		private $_table="mahasiswa";

		public $id_mhs;
		public $nama_mhs;
		public $nim_mhs;
		public $kelas_mhs;
		public $jurusan_mhs;
		public $prodi_mhs;
		public $alamat_mhs;
		public $telp_mhs;

		public function rules()
		{
			return[
				[
					'field'=>'nama_mhs',
					'label'=>'Nama Mahasiswa',
					'rules'=>'required'
				],
				[
					'field'=>'nim_mhs',
					'label'=>'Nomor Induk Mahasiswa',
					'rules'=>'required'
				],
				[
					'field'=>'kelas_mhs',
					'label'=>'Kelas',
					'rules'=>'required'
				],
				[
					'field'=>'jurusan_mhs',
					'label'=>'Jurusan',
					'rules'=>'required'
				],
				[
					'field'=>'prodi_mhs',
					'label'=>'Prodi',
					'rules'=>'required'
				],
				[
					'field'=>'alamat_mhs',
					'label'=>'Alamat',
					'rules'=>'required'
				],
				[
					'field'=>'telp_mhs',
					'label'=>'Telepon',
					'rules'=>'required'
				],
			];
		}

		public function join($tbl,$tbljoin1,$tbljoin2,$join1,$join2,$type)
		{
			return $this->db->from($tbl)
			->join($tbljoin1,$join1,$type)
			->join($tbljoin2,$join2,$type)
			->order_by('nama_mhs','asc')
			->get()
			->result();
		}
		public function joinWhere($tbl,$tbljoin1,$tbljoin2,$join1,$join2,$where)
		{
			return $this->db
			->join($tbljoin1,$join1)
			->join($tbljoin2,$join2)
			->get_where($tbl,["id_mhs"=>$where])
			->row();
		}

		public function getAll()
	    {
	        return $this->db->get($this->_table)->result();
	    }

		public function getById($id)
		{
			return $this->db->get_where($this->_table,["id_mhs"=>$id])->row();
		}

		function fetch_jurusan()
		{
		 	$this->db->order_by("id_jurusan", "ASC");
		  	$query = $this->db->from('jurusan');
		  	return $query->get()->result();
		}

		function fetch_prodi($id_jurusan)
		{
			$this->db->where('id_jurusan', $id_jurusan);
			$this->db->order_by('id_prodi', 'ASC');
			$query = $this->db->from('prodi');
			// ->join('mahasiswa','mahasiswa.prodi_mhs=prodi.id_prodi','left');
			$output = '<option value="">Pilih Prodi</option>';
			foreach($query->get()->result() as $row)
			{
			 $output .= '<option value="'.$row->id_prodi.'">'.$row->nama_prodi.'</option>';
			}
			return $output;
		}

		public function save()
		{
			$post=$this->input->post();
			$this->id_mhs=uniqid();
			$this->nama_mhs=$post["nama_mhs"];
			$this->nim_mhs=$post["nim_mhs"];
			$this->kelas_mhs=$post["kelas_mhs"];
			$this->jurusan_mhs=$post["jurusan_mhs"];
			$this->prodi_mhs=$post["prodi_mhs"];
			$this->alamat_mhs=$post["alamat_mhs"];
			$this->telp_mhs=$post["telp_mhs"];
			return $this->db->insert($this->_table,$this);
		}

		public function update()
		{
			$post=$this->input->post();
			$this->id_mhs=$post["id_mhs"];
			$this->nama_mhs=$post["nama_mhs"];
			$this->nim_mhs=$post["nim_mhs"];
			$this->kelas_mhs=$post["kelas_mhs"];
			$this->jurusan_mhs=$post["jurusan_mhs"];
			$this->prodi_mhs=$post["prodi_mhs"];
			$this->alamat_mhs=$post["alamat_mhs"];
			$this->telp_mhs=$post["telp_mhs"];
			return $this->db->update($this->_table,$this,array('id_mhs'=> $post['id_mhs']));
		}

		public function delete($id)
		{
			return $this->db->delete($this->_table,array("id_mhs"=>$id));
		}

		public function data_null($output,$text)
		{
			if($output==null)
			{
				echo "<b>Silahkan isi Data ".$text."</b>";
			}
			else
			{
				echo $output;
			}
		}

	}

 ?>