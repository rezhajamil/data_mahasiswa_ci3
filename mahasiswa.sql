-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 12, 2021 at 02:04 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mahasiswa`
--

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `nama_jurusan` varchar(40) NOT NULL,
  `kode_jurusan` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `nama_jurusan`, `kode_jurusan`) VALUES
(1, 'Teknik Komputer dan Informatika', 'TKI'),
(2, 'Teknik Elektro', 'TKE'),
(3, 'Teknik Mesin', 'TME'),
(8, 'Teknik Sipil', 'TSI'),
(9, 'Akuntansi Keuangan', 'AAK');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id_mhs` varchar(15) NOT NULL,
  `nama_mhs` varchar(25) NOT NULL,
  `nim_mhs` varchar(15) NOT NULL,
  `jurusan_mhs` int(11) DEFAULT NULL,
  `prodi_mhs` int(11) DEFAULT NULL,
  `kelas_mhs` varchar(7) NOT NULL,
  `alamat_mhs` varchar(60) NOT NULL,
  `telp_mhs` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id_mhs`, `nama_mhs`, `nim_mhs`, `jurusan_mhs`, `prodi_mhs`, `kelas_mhs`, `alamat_mhs`, `telp_mhs`) VALUES
('6047245a6fb7d', 'Rezha', '1805102025', 1, 17, '6C', 'Padang Bulan Selayang, Medan', '081269863028'),
('604abbc43e1af', 'Budi', '1805102022', 2, 8, '6D', 'Medan Polonia, Medan', '081345123463'),
('604abcd54d620', 'Aryo', '1805102030', 9, 21, '6E', 'Medan Baru, Medan', '081245982987'),
('604abd60908cf', 'Galih', '1905102133', 8, 20, '4C', 'Medan Tembung, Medan', '081922110234');

-- --------------------------------------------------------

--
-- Table structure for table `prodi`
--

CREATE TABLE `prodi` (
  `id_prodi` int(11) NOT NULL,
  `nama_prodi` varchar(50) NOT NULL,
  `kode_prodi` varchar(5) NOT NULL,
  `id_jurusan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prodi`
--

INSERT INTO `prodi` (`id_prodi`, `nama_prodi`, `kode_prodi`, `id_jurusan`) VALUES
(2, 'Teknik Komputer', 'CE', 1),
(3, 'Teknik Elektronika', 'EK', 2),
(8, 'Teknik Telekomunikasi', 'TK', 2),
(9, 'Multimedia', 'MM', 1),
(10, 'Teknik Mesin', 'ME', 3),
(11, 'Teknik Konversi Energi', 'EN', 3),
(17, 'Manajemen Informatika', 'MI', 1),
(18, 'Teknik Listrik', 'EL', 2),
(19, 'Teknik Sipil', 'SI', 8),
(20, 'Manajemen Rekayasa Konstruksi Gedung', 'MRKG', 8),
(21, 'Administrasi Niaga', 'AN', 9),
(22, 'Akuntansi', 'AK', 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id_mhs`),
  ADD KEY `jurusan_mhs` (`jurusan_mhs`),
  ADD KEY `prodi_mhs` (`prodi_mhs`);

--
-- Indexes for table `prodi`
--
ALTER TABLE `prodi`
  ADD PRIMARY KEY (`id_prodi`),
  ADD KEY `id_jurusan` (`id_jurusan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `prodi`
--
ALTER TABLE `prodi`
  MODIFY `id_prodi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`jurusan_mhs`) REFERENCES `jurusan` (`id_jurusan`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `mahasiswa_ibfk_2` FOREIGN KEY (`prodi_mhs`) REFERENCES `prodi` (`id_prodi`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `prodi`
--
ALTER TABLE `prodi`
  ADD CONSTRAINT `prodi_ibfk_1` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id_jurusan`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
